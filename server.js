const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.static());

const port = 8000;

mongoose.connect('mongodb://localhost',{useNewUrlParser: true}).then(client =>{
    app.use('/', link);
    app.listen(port, () => {
        console.log(`Server started on ${port} port`);
    });
});