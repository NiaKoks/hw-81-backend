const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const LinkSchema = new Schema({
    _id:{
        type: String,
        required:true
    },
    shortURL: String,
    fullURL: {
        type: String,
        required: true}
});
const Link = mongoose.model('Link',LinkSchema);
module.exports = Link;