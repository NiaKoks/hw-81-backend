const Links = require('../models/Link');
const router = express.Router();

    router.get('/links', async (req, res) => {
        const urlCode = req.params.code;
        const item = await Links.findOne({ urlCode: urlCode });
        if (item) {
            return res.redirect(item.fullURL);
        } else {
            return res.redirect(constants.errorUrl);
        }
    });

    router.post('/:shortURL', async (req, res) => {
        const { shortBaseUrl, fullURL: fullURL } = req.body;
        const queryOptions = { fullURL: fullURL };
        if (fullURL !==" ") {
            let urlData;
            try {
                if (!urlData) {
                    urlData = await Links.findOne(queryOptions).exec();
                }
                if (urlData) {
                    res.status(200).json(urlData);
                } else {
                    const urlCode = shortCode.generate();
                    return shortBaseUrl + '/' + urlCode;
                }
            } catch (err) {
                res.status(401).json('Invalid Id');
            }
        } else {
            return res.status(401).json('Invalid Original Url.');
        }
    });
module.exports = router;
